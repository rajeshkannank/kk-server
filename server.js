const express = require('express');
const http = require('http');
const app = express();
var jsonfile = require('jsonfile');
var bodyParser = require('body-parser');
var patients = require('./mock/patients.js');
var patientsName = require('./mock/patientsName.js');
var doctors = require('./mock/doctors.js');
var doctorsName = require('./mock/doctorsName.js');
var users = require('./mock/users.js');
var testGroup = require('./mock/testGroup.js');
var orderIds = require('./mock/orders.js');
//var thisOrder = require('./mock/Order-1_59314509e1a81797112615b7.js');
app.use(bodyParser.urlencoded({ extended: false }));
 
// parse application/json 
app.use(bodyParser.json());

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3030');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.get('/', function (req, res) {
  res.send('Hello World');
});

app.post('/authenticate', function (req, res) {
	console.log('my post 123');
	console.log(req.body.login, req.body.password);
  if(req.body.login == "admin" && req.body.password == "admin"){
    res.send({id : 'user-1',login : 'admin',email : 'admin@demo.com',token : 'admin-myToken'});
  }
  else{
    res.send(401);
  }
});
app.post('/orders', function(req, res){
  console.log('orderedTest ',req.body);
  console.log('orderedTest ',req.body.Order);
  res.send('Order success');
});
app.get('/patients', function(req, res){
  console.log('patients');
  //res.send(patientsName);
  res.send(patients);
});
app.get('/doctors', function(req, res){
  console.log('doctors');
  //res.send(doctorsName);
  res.send(doctors);
});

app.get('/testGroups', function(req, res){
  console.log('testGroup');
  res.send(testGroup);
});

app.get('/orders', function(req, res){
  console.log(req.query.orderid);
  console.log('get orders');
  if(req.query.orderid == undefined || req.query.orderid == 'undefined'){
    res.send(orderIds);
  }else{
    jsonfile.readFile('./mock/Order-1_59314509e1a81797112615b7.json', function(err, obj){
      res.send(obj);
    });
  }
});
app.put('/orders/completePayment', function(req, res){
  console.log('put payment');
  res.send('Payment completed');
});

app.put('/orders', function(req, res){
  console.log('put orders ', req.body);
  jsonfile.writeFile('./mock/Order-1_59314509e1a81797112615b7.json', req.body.orders, function (err) {
    console.error(err);
  });
  res.send('order is success');
});
app.put('/collectSample', function(req, res){
  res.send('sample collected successfully');
});

app.listen(process.env.PORT || 3000, function () {

  console.log("Example app listening at ", 3000);

});